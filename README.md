# Zima_Bluescreen
A fun script to generate Zima Bluescreen backgrounds.

Here is a fun zip folder for images: https://esahubble.org/images/archive/top100/

## Config format
zima_blue: string starting with "#" exp.: "#ffffff"

ratio: string with ":" separator and integers exp.: "16:10"

text_chance: string with percentage exp.: "94.3942%" or "95%" or as a string of number from 0 to 1 exp.: "1" or "0.2314" or .123 or 0

## Dependencies
PIL