from pprint import pprint

class Chunk:
    def __init__(self, f):
        self.reader = f
        self.chunk_length = int.from_bytes(f.read(4), "big")
        self.chunk_type = f.read(4)

        self.chunk_info = {}
        self.chunk_data = {}

    def set_info(self, chunk_info):
        self.chunk_info = chunk_info

    def read(self):
        remaining_data = self.chunk_length

        for key, length in self.chunk_info:
            if length <= remaining_data:
                self.chunk_data[key] = int.from_bytes(f.read(length), "big")
            else:
                break

            remaining_data -= length
        
        self.chunk_data["remaining_data"] = f.read(remaining_data)

        self.crc = f.read(4)
    
    def __repr__(self):
        return f'<Chunk Type:{self.chunk_type} Length:{self.chunk_length}>'
    
    def __bool__(self):
        return self.chunk_type != b'IEND'

with open("src/Zima_Bluescreen.png", "rb") as f:
    assert list(f.read(8)) == [137, 80, 78, 71, 13, 10, 26, 10]

    while (chunk := Chunk(f)):
        print(chunk)
        chunk.read()
    
    print(chunk)
