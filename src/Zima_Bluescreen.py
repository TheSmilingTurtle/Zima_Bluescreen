#!/bin/python3

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import random
import os
import json

######## SETUP #########
Image.MAX_IMAGE_PIXELS = float("inf")

DIR_PATH = os.path.dirname(os.path.realpath(__file__))

with open(DIR_PATH + "/conf.json") as conf:
    CONFIG = json.load(conf)

ZIMA_BLUE = CONFIG["zima_blue"]
TEXT_COLOUR = CONFIG["text_colour"]

RATIO = tuple([int(x) for x in CONFIG["ratio"].split(":")])

TEXT_CHANCE_RAW = CONFIG["text_chance"]
if isinstance(TEXT_CHANCE_RAW, str) and TEXT_CHANCE_RAW.endswith("%"):
    TEXT_CHANCE = float(TEXT_CHANCE_RAW[:-1])/100
else:
    TEXT_CHANCE = float(TEXT_CHANCE_RAW)

BACKGROUND_DIR = CONFIG["background_dir"]

with open(DIR_PATH + "/texts.json") as conf:
    TEXTS = json.load(conf)

FACES = [x[1] for x in TEXTS["faces"]]
FACES_WEIGHTS = [x[0] for x in TEXTS["faces"]]
MIDDLE_TEXT = [x[1] for x in TEXTS["middle_text"]]
MIDDLE_TEXT_WEIGHTS = [x[0] for x in TEXTS["middle_text"]]
BOTTOM_TEXT = [x[1] for x in TEXTS["bottom_text"]]
BOTTOM_TEXT_WEIGHTS = [x[0] for x in TEXTS["bottom_text"]]

######## LOADING #########
BACKGROUNDS = [BACKGROUND_DIR + "/" + x for x in os.listdir(BACKGROUND_DIR) if x.endswith(".png") or x.endswith(".jpg") or x.endswith(".jpeg")]
BACKGROUNDS_WEIGHTS = CONFIG["background_weights"]

assert len(BACKGROUNDS) != 0

if len(BACKGROUNDS_WEIGHTS) == len(BACKGROUNDS):
    background = random.choices(BACKGROUNDS, weights=BACKGROUNDS_WEIGHTS)
else:
    background = random.choice(BACKGROUNDS)

im = Image.open(background)
width, height = im.size

######## CROPPING #########
w_div = width/RATIO[0]
h_div = height/RATIO[1]

if w_div != h_div:
    if w_div > h_div:
        new_width = h_div * RATIO[0]

        crop_window = ( int((width - new_width)/2), 0, width - int((width - new_width)/2), height)
    else:
        new_height = w_div * RATIO[1]

        crop_window = ( 0, int((height - new_height)/2), width, height - int((height - new_height)/2))
    
    im = im.crop( crop_window )

width, height = im.size

######## RECTANGLE #########
rectangle_pos = ( int(1/4 * width), int(1/4 * height), int(3/4 * width), int(3/4 * height))
im.paste(ZIMA_BLUE, rectangle_pos)

######## TEXT #########

rng = random.random()

if rng < TEXT_CHANCE:

    ######## FONTS #########

    FACE_FONT = CONFIG["face_font"]
    MIDDLE_TEXT_FONT = CONFIG["middle_text_font"]
    BOTTOM_TEXT_FONT = CONFIG["bottom_text_font"]

    FACE_FONT_SIZE = int(height/7)
    MIDDLE_FONT_SIZE = int(height/37)
    BOTTOM_FONT_SIZE = int(height/48)

    FACE_FONT = ImageFont.truetype(FACE_FONT, FACE_FONT_SIZE)
    MIDDLE_FONT = ImageFont.truetype(MIDDLE_TEXT_FONT, MIDDLE_FONT_SIZE)
    BOTTOM_FONT = ImageFont.truetype(BOTTOM_TEXT_FONT, BOTTOM_FONT_SIZE)

    face = random.choices(FACES, weights=FACES_WEIGHTS)[0]
    middle = random.choices(MIDDLE_TEXT, weights=MIDDLE_TEXT_WEIGHTS)[0]
    bottom = random.choices(BOTTOM_TEXT, weights=BOTTOM_TEXT_WEIGHTS)[0]

    ######## Drawing #########
    I = ImageDraw.Draw(im)

    I.text( ( int(1/28 * width) + int(1/4 * width), int(1/4 * height)), face, TEXT_COLOUR, font=FACE_FONT)
    I.text( ( int(1/48 * width) + int(1/4 * width), FACE_FONT_SIZE + MIDDLE_FONT_SIZE + int(1/48 * height) + int(1/4 * height)), middle, TEXT_COLOUR, font=MIDDLE_FONT)
    I.text( ( int(1/48 * width) + int(1/4 * width), height - int(1/3 * height)), bottom, TEXT_COLOUR, font=BOTTOM_FONT)


FILE_PATH = DIR_PATH + "/Zima_Bluescreen.png"

im.save(FILE_PATH)

del im

os.system(f'gsettings set org.gnome.desktop.background picture-uri-dark {FILE_PATH}')